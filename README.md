# Bedside Connect / RoomService Connect

## Cookbook
* [Prerequisites](#prerequisites) 
* [Project setup](#setup)
* [Running BSC/RSC](#running)
* [Create publish package](#create-publish-package)

## Prerequisites
- Visual Studio 2017. Make sure to install the following components/workloads:
	* ASP.NET and web development
	* Node.js build tools
- Visual Studio Code
- git
- .net framework 4.7.2 SDK
- .net framework 4.7.2 Targeting Pack
- npm 5+

## Setup {#setup}
1. Clone this repository
2. Open command prompt in the root folder
3. execute `git submodule update --init --recursive`
4. Change directory to BedsideConnect/ci folder
5. execute `npm install`
6. Install workspace recommended extensions for Visual Studio Code:
	1. Open CI.Connect.Inventory.Web\ci folder in VS code 
	2. Go to View -> Extensions 
	3. Press the three dots on top-right of the new window and select "Show Recommended Extensions"
	4. A list with the workspace recommended extensions appears and you cand install them from there
You can find more details at https://code.visualstudio.com/docs/editor/extension-gallery#_workspace-recommended-extensions 


<!-- ## Prepare static files {#prepare-static-files}
1. Open command prompt in the CI.Connect.Inventory.Web/ci folder
2. execute `npm run build-inventory-static_files`
3. execute `npm run build --prod inventory` -->


## Run using Visual Studio 2017 {#running}
1. Open command prompt in BedsideConnect/ci folder or open this folder in Visual Studio Code
2. Go back to root folder and open BedisdeConnect.sln 
3. Right click on solution in solution explorer. Go to properties. Select multiple startup projects: Check BedsideConnect and BedsideConnectApi.
4. Go to BedsideConnect folder and run `npm install`.
5. Open Task Runner Explorer in Visual Studio and run buildAll task or go to BedsideConnect folder open a command prompt and run `gulp buildAll`. If gulp is not recognized as a command even if it was installed locally due to previous npm install please also install gulp globally by running `npm install -g gulp` and then run again `gulp buildAll`. 
6. Add OracleDataAccess to GAC. This is required after Oracle 12c client installation. See steps here: https://dev.azure.com/CIDevOps1/CI/_wiki/wikis/CI.wiki/161/Commands-to-Run-after-the-Oracle-Client-12c-Release-2-installation 
7. Make sure the HOST name you specified at login exists in tnsnames.ora file. The location of the tnsnames.ora file is ORACLE_HOME\network\admin. If there is no tnsnames.ora file you have to create it and then add the connection strings to it.   
1. Example:
    ~~~
      YOUR_HOST_NAME = (DESCRIPTION =
						(ADDRESS_LIST =
						(ADDRESS = (PROTOCOL = TCP)(HOST = DBSERVER_HOST)(PORT = 1521))
						)
						(CONNECT_DATA =
						(SERVICE_NAME = hs)
						)
					)
2. You have to specify the corect HOST,PORT and SERVICE_NAME.
3. Replace YOUR_HOST_NAME with a host name. 				
8. If the SCHEMA you are connecting to is SSO enabled then you have to specify the connection strings in BedsideConnect.Api/appsettings.json file.

   
1. Example:
   ~~~
	 YOUR_HOST_NAME: {
      "ConnectionString": "Data Source=(DESCRIPTION=(ADDRESS=(PROTOCOL=tcp)(HOST=DBSERVER_HOST)(PORT=1521))(CONNECT_DATA=(SERVICE_NAME=hs)))",
      "Schemas": {
        "SCHEMA_NAME": {
          "FacilityId": "SCHEMA_NAME",
          "ServiceUserName": "SERVICE_USER_NAME",
          "ServiceUserPassword": "decrypted=THE_DECRYPTED_PASSWORD"
        },
      }
    },
2. Replace 'YOUR_HOST_NAME' with the name of your host.
3. The 'YOUR_HOST_NAME' from tnsnames.ora (step 7) should have the same connection string configuration as the 'YOUR_HOST_NAME' from appsettings.json
4. Replace 'SCHEMA_NAME' with the schema you want to connect to 
5. Replace 'SERVICE_USER_NAME' with the id of your service user
6. Replace 'THE_DECRYPTED_PASSWORD' with plain text password of your service user.	 
 
9. Return to Visual Studio and click Start.
10. The applications are started in IIS express. Check that the BedsideConnect app is running on http://localhost:51768 and BedsideConnect.Api app is running on http://localhost:52001
		
		

## Steps after modifying the code {#steps-to-merge}
1. Commit and Push the changes, make sure to link the commit to a story ( the commit message should start with #StoryId)
2. Create a pull request in Azure
    * If one of the submodules was modified, make a pull request on the submodule and after the code is merged create the pull request on Room Service Connect repository with the updated commit id of the submodule 
    * On CI.Connect.Client.Workspace submodule there are some steps which are executed during gated checkin. In case that one of the steps failed you can check locally if the error was fixed by running the afferent command (commands need to be executed in CI.Connect.Inventory.Web/ci folder)
      * Run Format: execute `npm run format-ts:check`
      * Run Affected Lint: execute `npm run affected:lint --parallel`
      * Run Lint for all projects: execute `npm run affected:lint --all --parallel`
      * Build Affected Apps: `npm run affected:build --parallel --exclude=inventory --prod`
      * Build all projects: `npm run affected:build --all --parallel --exclude=inventory --prod`
      * Test Affected Apps: `npm run affected:test --code-coverage --exclude=hs-hs-api,hs-schema-version,bsc-legacy,ic-requisitions,hs-api-settings,bsc`
      * Test all projects: `npm run affected:test --all --code-coverage --exclude=hs-hs-api,hs-schema-version,bsc-legacy,ic-requisitions,hs-api-settings,bsc`
    * Redo step 1 if errors from gated checkin were fixed


## Creating a publish package using Azure {create-publish-package}
1. Go to https://dev.azure.com/CIDevOps1/CI/_build?definitionId=92 and get the artifacts from last build (after you merged the code in the develop branch)
	*you will find the files in CI.Connect.Inventory.Web.zip
	
